package com.sablania.flightsearchapp.utils;

import com.sablania.flightsearchapp.models.FlightModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by ViPul Sublaniya on 27-09-2017.
 */

public class CompareFlightObject implements Comparator<FlightModel> {

    int param;
    public CompareFlightObject(int param) {
        this.param = param;
    }

    @Override
    public int compare(FlightModel lhs, FlightModel rhs) {

        if (param==1) {
            try {
                int lhsMinPrice = new JSONObject(lhs.getFares().get(0).toString()).getInt("fare");
                int rhsMinPrice=new JSONObject(rhs.getFares().get(0).toString()).getInt("fare");
                for (Object o : lhs.getFares()) {
                    JSONObject jo = new JSONObject(o.toString());
                    if(jo.getInt("fare")<lhsMinPrice){
                        lhsMinPrice = jo.getInt("fare");
                    }
                }
                for (Object o : rhs.getFares()) {
                    JSONObject jo = new JSONObject(o.toString());
                    if(jo.getInt("fare")<rhsMinPrice){
                        rhsMinPrice = jo.getInt("fare");
                    }
                }
                if (rhsMinPrice > lhsMinPrice) {
                    return -1;
                }else {
                    return 1;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (param==2) {
            return String.valueOf(lhs.getDepartureTime()).compareTo(rhs.getDepartureTime() + "");
        }else if (param==3) {
            return String.valueOf(rhs.getDepartureTime()).compareTo(lhs.getDepartureTime() + "");
        } else if(param==4){
            return String.valueOf(lhs.getArrivalTime()).compareTo(rhs.getArrivalTime() + "");
        } else if(param==5){
            return String.valueOf(rhs.getArrivalTime()).compareTo(lhs.getArrivalTime() + "");
        }
        return 0;

    }
}
