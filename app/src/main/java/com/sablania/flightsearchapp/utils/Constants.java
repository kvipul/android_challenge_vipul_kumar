package com.sablania.flightsearchapp.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.sablania.flightsearchapp.BuildConfig;

import java.util.HashMap;

public class Constants {

    public static HashMap<String, String> getHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public static void handleVolleyError(Context context, VolleyError error, boolean showToast){
        if(BuildConfig.DEBUG) {
            if (error.networkResponse != null)
                Log.e("Volley Error Code- ", error.networkResponse.statusCode + "");
            Log.e("Volley Error Msg - ", error.toString() + " in " + context.getClass().getName());
        }
        if (error instanceof NoConnectionError) {
            if(showToast){
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }else if(error instanceof TimeoutError){
            if(showToast){
                Toast.makeText(context, "Connection Timeout! Please try again.", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(showToast){
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

