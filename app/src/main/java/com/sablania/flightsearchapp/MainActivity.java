package com.sablania.flightsearchapp;

import android.app.ProgressDialog;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.flightsearchapp.adapters.FlightAdapter;
import com.sablania.flightsearchapp.models.AppendixModel;
import com.sablania.flightsearchapp.models.FlightModel;
import com.sablania.flightsearchapp.utils.AppController;
import com.sablania.flightsearchapp.utils.CompareFlightObject;
import com.sablania.flightsearchapp.utils.Constants;
import com.sablania.flightsearchapp.utils.GenericRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    AppendixModel appendixModel;

    RecyclerView rvFlights;
    TextView noData;
    Button tryAgain;
    AppCompatSpinner spnSort;
    FlightAdapter flightAdapter;

    List<FlightModel> dataList;
    String API = "http://www.mocky.io/v2/5979c6731100001e039edcb3";
    boolean cacheUsed;

    OrientationEventListener orientationEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();
        fetchData();

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchData();
            }
        });
        spnSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    fetchData();
                    return;
                }
                if(dataList!=null) {
                    Collections.sort(dataList, new CompareFlightObject(position));
                    flightAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        orientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {
                spnSort.setSelection(0);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, "No setting added!", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        rvFlights = (RecyclerView) findViewById(R.id.rv_flights);
        noData = (TextView) findViewById(R.id.tv_no_data);
        tryAgain = (Button) findViewById(R.id.try_again);
        spnSort = (AppCompatSpinner) findViewById(R.id.spn_sort);

        List<String> sortOptions = new ArrayList<>();
        sortOptions.add("None");
        sortOptions.add("Cheap");
        sortOptions.add("Departure ascending");
        sortOptions.add("Departure descending");
        sortOptions.add("Arrival ascending");
        sortOptions.add("Arrival descending");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sortOptions);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSort.setAdapter(dataAdapter);
        spnSort.setSelection(0, true);
    }

    private void fetchData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        cacheUsed = false;

        //custom generic request for caching data
        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                API, String.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                cacheUsed = true;
                JSONObject jsonResponse;
                Gson gson = new Gson();

                try {
                    jsonResponse = new JSONObject(response.toString());

                    Type listType = new TypeToken<List<FlightModel>>() {
                    }.getType();

                    dataList = gson.fromJson(jsonResponse.getString("flights"), listType);
                    appendixModel = gson.fromJson(jsonResponse.getString("appendix"), AppendixModel.class);

                } catch (JSONException e) {
                    e.printStackTrace();
                }



                flightAdapter = new FlightAdapter(MainActivity.this, dataList, appendixModel);
                rvFlights.setAdapter(flightAdapter);
                rvFlights.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                if (dataList.isEmpty()) {
                    //if list is empty
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }
                progressDialog.dismiss();

                tryAgain.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(MainActivity.this, error, true);
                progressDialog.dismiss();
                if(!cacheUsed){
                    //if no internet
                    tryAgain.setVisibility(View.VISIBLE);
                }
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "flights");
    }

}
