package com.sablania.flightsearchapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sablania.flightsearchapp.R;
import com.sablania.flightsearchapp.models.AppendixModel;
import com.sablania.flightsearchapp.models.FlightModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    private List<FlightModel> list;
    private AppendixModel appendixModel;
    private int VISIBLE_PROVIDERS = 2;

    public FlightAdapter(Context context, List<FlightModel> list, AppendixModel appendixModel) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.appendixModel = appendixModel;
    }

    @Override
    public FlightAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.cv_flight_list_item, parent, false);
        FlightAdapter.ViewHolder holder = new FlightAdapter.ViewHolder(convertView);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FlightAdapter.ViewHolder holder, final int position) {
        final FlightModel flightModel = list.get(position);

        holder.tvAirline.setText(appendixModel.getAirlines().get(flightModel.getAirlineCode()));
        holder.tvOriginCode.setText(flightModel.getOriginCode());
        holder.tvDestinationCode.setText(flightModel.getDestinationCode());
        holder.tvOriginAirport.setText(appendixModel.getAirports().get(flightModel.getOriginCode()));
        holder.tvDestinationAirport.setText(appendixModel.getAirports().get(flightModel.getDestinationCode()));

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        holder.tvDepartureTime.setText(sdf.format(new Date(flightModel.getDepartureTime())));
        holder.tvArrivalTime.setText(sdf.format(new Date(flightModel.getArrivalTime())));

        holder.llProvidersContainer.removeAllViews();
        if (flightModel.getFares()!=null && !flightModel.getFares().isEmpty()) {
            holder.tvProvidersLabel.setVisibility(View.VISIBLE);
            holder.tvSeeMoreProviders.setVisibility(View.GONE);

            if (flightModel.getFares().size() > VISIBLE_PROVIDERS) {
                holder.llProvidersContainer.addView(getProviders(flightModel, VISIBLE_PROVIDERS));
                holder.tvSeeMoreProviders.setVisibility(View.VISIBLE);
            } else {
                holder.llProvidersContainer.addView(getProviders(flightModel, flightModel.getFares().size()));
            }
        }

        holder.tvSeeMoreProviders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llProvidersContainer.removeAllViews();
                if (holder.tvSeeMoreProviders.getText().toString().equals("See More")) {
                    holder.tvSeeMoreProviders.setText("See Less");
                    holder.llProvidersContainer.addView(getProviders(flightModel, flightModel.getFares().size()));
                } else {
                    holder.tvSeeMoreProviders.setText("See More");
                    holder.llProvidersContainer.addView(getProviders(flightModel, VISIBLE_PROVIDERS));
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set action
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvAirline, tvOriginCode, tvDestinationCode, tvDepartureTime, tvArrivalTime, tvOriginAirport,
                tvDestinationAirport, tvSeeMoreProviders, tvProvidersLabel;
        LinearLayout llProvidersContainer;

        ViewHolder(View itemView) {
            super(itemView);

            tvAirline = (TextView) itemView.findViewById(R.id.tv_airline);
            tvOriginCode = (TextView) itemView.findViewById(R.id.tv_origin_code);
            tvDestinationCode = (TextView) itemView.findViewById(R.id.tv_destination_code);
            tvDepartureTime = (TextView) itemView.findViewById(R.id.tv_departure_time);
            tvArrivalTime = (TextView) itemView.findViewById(R.id.tv_arrival_time);
            tvOriginAirport = (TextView) itemView.findViewById(R.id.tv_origin_airport);
            tvDestinationAirport = (TextView) itemView.findViewById(R.id.tv_destination_airport);
            tvSeeMoreProviders = (TextView) itemView.findViewById(R.id.tv_see_more);
            tvProvidersLabel = (TextView) itemView.findViewById(R.id.tv_providers_and_fares);
            llProvidersContainer = (LinearLayout) itemView.findViewById(R.id.ll_providers_container);
        }
    }

    private LinearLayout getProviders(FlightModel flightModel, int count) {
        View view;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        try {
            for (int i = 0; i < count; i++) {
                view = inflater.inflate(R.layout.provider_detail_item, null, false);

                JSONObject jsonObject = new JSONObject(flightModel.getFares().get(i).toString());
                ((TextView) view.findViewById(R.id.tv_provider)).setText(appendixModel.getProviders().get(jsonObject.getInt("providerId")+""));
                ((TextView) view.findViewById(R.id.tv_provider_fare)).setText(jsonObject.getString("fare"));
                linearLayout.addView(view);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return linearLayout;
    }
}
