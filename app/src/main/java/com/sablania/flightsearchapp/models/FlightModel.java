package com.sablania.flightsearchapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ViPul Sublaniya on 27-09-2017.
 */
public class FlightModel implements Serializable {

    @SerializedName("originCode")
    String originCode;

    @SerializedName("destinationCode")
    String destinationCode;

    @SerializedName("departureTime")
    long departureTime;

    @SerializedName("arrivalTime")
    long arrivalTime;

    @SerializedName("fares")
    List<Object> fares;

    @SerializedName("airlineCode")
    String airlineCode;

    @SerializedName("class")
    String classz;

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<Object> getFares() {
        return fares;
    }

    public void setFares(List<Object> fares) {
        this.fares = fares;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getClassz() {
        return classz;
    }

    public void setClassz(String classz) {
        this.classz = classz;
    }
}
