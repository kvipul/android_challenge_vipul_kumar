package com.sablania.flightsearchapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by ViPul Sublaniya on 27-09-2017.
 */

public class AppendixModel implements Serializable {

    @SerializedName("airlines")
    HashMap<String, String> airlines;

    @SerializedName("airports")
    HashMap<String, String> airports;

    @SerializedName("providers")
    HashMap<String, String> providers;

    public HashMap<String, String> getAirlines() {
        return airlines;
    }

    public void setAirlines(HashMap<String, String> airlines) {
        this.airlines = airlines;
    }

    public HashMap<String, String> getAirports() {
        return airports;
    }

    public void setAirports(HashMap<String, String> airports) {
        this.airports = airports;
    }

    public HashMap<String, String> getProviders() {
        return providers;
    }

    public void setProviders(HashMap<String, String> providers) {
        this.providers = providers;
    }
}
