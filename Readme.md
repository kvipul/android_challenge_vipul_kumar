#Pseudo Flight Search App
Development Time: 3.5 Hrs

##Aim
Build a Flight Search app​ that should display a list of sample flights between
Delhi and Mumbai. Each flight in the list should have fares from multiple booking providers

##App Features
- Home screen displays flight results from the api with along sort options
- To make result view better, initially only two providers will be visible, user can click on show more button to see more providers.
- Views are made in such a way that app will work fine in both orientation
- GenericRequest is used to cache the api response for a few day, with this app will work in offline mode also
- Code has been written in separate modules to remove code duplication, maintain better code structure and to make app easily scalable
- Proguard is also used for release build to make app size small and to make app more secure

##Third Party libraries and References
- Volley: For Network Call
- Data Source: [Ixigo link](http://www.mocky.io/v2/5979c6731100001e039edcb3)
- Android Support Library
- Some Files like GenericRequest.java, AppController.java are the files that I've been using for a very long time. Some section of these files may be from internet
 